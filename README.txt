CKEDITOR Case Change - A PLUGIN TO EASILY Change the case of selected text in ckeditor
Project link will be coming soon


-- REQUIREMENTS --

Currently, CKEDITOR Case Change needs the following to run:

Wysiwyg module, version 6.x-2.1 or above
the CKEditor editor, version 3.5.3 or above.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

Copy the ckeditor_casechange folder to your sites/all/modules directory.
Navigate to admin/build/modules and enable the module.


-- CONFIGURATION --

* Go to Administer > Site configuration > Wysiwyg profiles and

* Edit CKEditor profile (click on the Edit button next to the input format CKEditor is assigned to).

* Under Buttons and plugins, check lowercase, uppercase and smallcaps checkbox.

* Save changes.  

-- CONTACT --

Current maintainer:
* Abdul Hafeez (rightchoice2c_me) - http://drupal.org/user/315349
