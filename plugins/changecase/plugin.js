CKEDITOR.plugins.add('changecase',
{
	init: function ( editor )
	{
		editor.addCommand('lowercase', CKEDITOR.plugins.lowercase);
		editor.ui.addButton( 'lowercase',
		{
			// Toolbar button tooltip.
			label: 'Lower Case',
			// Reference to the plugin command name.
			command: 'lowercase',
			// Button's icon file path.			
			icon: this.path + 'images/img.jpg'
		});

		editor.addCommand('uppercase', CKEDITOR.plugins.uppercase);
		editor.ui.addButton( 'uppercase',
		{
			// Toolbar button tooltip.
			label: 'Upper Case',
			// Reference to the plugin command name.
			command: 'uppercase',
			// Button's icon file path.			
			icon: this.path + 'images/icon.jpg'
		});

		editor.addCommand('smallcaps', CKEDITOR.plugins.smallcaps);
		editor.ui.addButton( 'smallcaps',
		{
			// Toolbar button tooltip.
			label: 'Small Caps',
			// Reference to the plugin command name.
			command: 'smallcaps',
			// Button's icon file path.			
			icon: this.path + 'images/smallcaps.jpg'
		});

	}

});

CKEDITOR.plugins.lowercase =
{
	exec : function( editor )
	{		
		if (editor.getSelection().getStartElement().is('span') && editor.getSelection().getStartElement().getAttribute('class') == "cke-element-lowercase") {
			editor.getSelection().getStartElement().setAttribute('style', '');
			editor.getSelection().getStartElement().setAttribute('class', '');
		} else {
			if (CKEDITOR.env.ie) {
			   selectedText = editor.getSelection().document.$.selection.createRange().text;
			} else {
				selectedText  = editor.getSelection().getNative();
			} 
			var selection = editor.getSelection(),	
			ranges = selection.getRanges( true );	
			var span = editor.document.createElement( 'span' );
			span.setAttribute('class', 'cke-element-lowercase');	
			span.setAttribute('style', 'text-transform: lowercase');
			span.appendHtml(selectedText.toString());
			editor.insertElement( span );
			selection.selectElement( span );			
		}
	}
};

CKEDITOR.plugins.uppercase =
{
	exec : function( editor )
	{
		if (editor.getSelection().getStartElement().is('span') && editor.getSelection().getStartElement().getAttribute('class') == "cke-element-uppercase") {
			editor.getSelection().getStartElement().setAttribute('style', '');
			editor.getSelection().getStartElement().setAttribute('class', '');
		} else {
			if (CKEDITOR.env.ie) {
			   selectedText = editor.getSelection().document.$.selection.createRange().text;
			} else {
				selectedText  = editor.getSelection().getNative();
			} 
			var selection = editor.getSelection(),	
			ranges = selection.getRanges( true );	
			var span = editor.document.createElement( 'span' );
			span.setAttribute('class', 'cke-element-uppercase');	
			span.setAttribute('style', 'text-transform: uppercase');
			span.appendHtml(selectedText.toString());
			editor.insertElement( span );
			selection.selectElement( span );			
		}
	}
};

CKEDITOR.plugins.smallcaps =
{
	exec : function( editor )
	{
		if (editor.getSelection().getStartElement().is('span') && editor.getSelection().getStartElement().getAttribute('class') == "cke-element-smallcaps") {
			editor.getSelection().getStartElement().setAttribute('style', '');
			editor.getSelection().getStartElement().setAttribute('class', '');
		} else {
			if (CKEDITOR.env.ie) {
			   selectedText = editor.getSelection().document.$.selection.createRange().text;
			} else {
				selectedText  = editor.getSelection().getNative();
			} 
			var selection = editor.getSelection(),	
			ranges = selection.getRanges( true );	
			var span = editor.document.createElement( 'span' );
			span.setAttribute('class', 'cke-element-smallcaps');	
			span.setAttribute('style', 'font-variant: small-caps;');
			span.appendHtml(selectedText.toString());
			editor.insertElement( span );
			selection.selectElement( span );			
		}
	}
};